Tree Server
===========

This program is intended to be run as a backend to serve trees. It
exposes a REST api to consult, create and change users and trees.


Initializing
------------

The default sql engine that it uses is `sqlite <https://www.sqlite.org/>`_,
with a local file named ``trees.db``. It can easily be changed to any other
(and should for scalability purposes).

Before running the backend the first time, you can initialize the database
this way::

  sqlite3 trees.db < create_tables.sql
  sqlite3 trees.db < sample_data.sql

Then you can run the backend directly with::

  ./server.py

which will start it in debug mode. For a more serious usage, you can run it
for example with `gunicorn <https://gunicorn.org/>`_, as in::

  gunicorn server:app

which will listen locally, or use ``-b 0.0.0.0:5000`` to listen to exterior
connections too.


Example calls
-------------

You can use ``curl`` to test the backend with commands like::

  curl -H "Content-Type: application/json" -X DELETE -u user2:123 \
    -w '\nReturn code: %{http_code}\n' http://localhost:5000/users/2

  curl -H "Content-Type: application/json" -X POST -u user2:123 \
    -d'{"id": 6, "name": "x1", "description": "x2"}' \
    -w '\nReturn code: %{http_code}\n' http://localhost:5000/trees

  curl -H "Content-Type: application/json" -X POST \
    -d '{"username": "user2", "password": "123"}' \
    -w '\nReturn code: %{http_code}\n' http://localhost:5000/login


To keep on going with bearer authentication, take the returned token and use
it in the next calls like::

  curl -H "Content-Type: application/json" -H "Authorization: Bearer $token" \
    -w '\nReturn code: %{http_code}\n' http://localhost:5000/users


Tests
-----

You can also run a bunch of tests with::

  pytest-3

which will run all the functions that start with ``test_`` in the file
``test_server.py``. You can also use the contents of that file to see
examples of how to use the api.


Api
---

The REST api has the following endpoints::

  /users
  /users/<id>
  /trees
  /trees/<id>
  /info
  /id/users/<username>
  /id/trees/<name>
  /login

They all support the GET method to request information. To **create** *users*
or *trees* use the POST method on the ``/users`` and ``/trees``
endpoints. To **modify** their values use the PUT method. To **delete** them
use the DELETE method.

The ``/info`` endpoint returns information about the currently logged user. The
``/id`` endpoint is useful to retrieve user and tree ids from usernames and
tree names.

Some of the endpoints and methods will require to be authenticated to use them.
You can use a registered user and password with Basic Authentication or Token
Authentication to access (you must use the ``/login`` endpoint first for that).

All the requests must send the information as json (with the
``Content-Type: application/json`` header). The responses are also json-encoded.

Most calls contain the *key* (property name) ``message`` in the response. If
the request was successful, its value will be ``ok``. If not, it will include
the text ``Error:`` with a description of the kind of error.

When creating a user or a profile an additional property ``id`` is returned,
with the id of the created object.

Finally, when using token authentication, the returned object contains the
properties ``id``, ``username``, ``name``, and (most importantly) ``token``
with the values referring to the successfully logged user. The value of
``token`` must be used in subsequent calls, with the header
``Authorization: Bearer <token>``, to stay logged as the same user.
